<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Usuarios;
use app\components\Mensaje;

class SiteController extends Controller
{
    

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    
    public function actionPrueba()
    {
        $datos="<script>alert('Recibiendo datos...');</script>";
        
        return $this->render("vista",[
            "datos"=>$datos
        ]);
    }
    public function actionPrueba2()
    {
        $datos="<script>alert('1');</script>";
        
        return $this->render("vista2",[
            "datos"=>$datos
        ]);
    }
    public function actionFormulario()
    {
        if($datos=Yii::$app->request->post()){
            return $this->render("vista2",[
                "datos" => $datos["usuario"]
            ]);
        }else{
            return $this->render("formulario");
        }
    }
    public function actionUsuarios()
    {
        $model=new Usuarios();
        if($model->load(Yii::$app->request->post())){
            return $this->render('mostrarUsuarios', ['model' => $model]);
        }else{
            return $this->render('formularioUsuarios',['model'=>$model]);
        }
    }
    public function actionMensaje()
    {
        return $this->render("mensaje",[
            "titulo"=>'Ejemplo de clase',
            "texto"=>'Texto de ejemplo'
        ]);
    }
    public function actionMensaje1()
    {
        return $this->render("mensaje1",[
            "titulo"=>'Ejemplo de clase',
            "pie"=>'Pie'
        ]);
    }
    public function actionMensaje2()
    {
        return $this->render("mensaje2",[
            "titulo"=>'Ejemplo de clase',
            "contenido"=>'Texto de ejemplo'
        ]);
    }
}
